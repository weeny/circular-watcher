<?php
namespace Weeny\Lib\CircularWatcher\Tests;

use PHPUnit\Framework\TestCase;
use Weeny\Contract\Exceptions\CircularReferenceWatcherExceptionInterface;
use Weeny\Lib\CircularWatcher\CircularReferenceWatcher;
use Weeny\Lib\CircularWatcher\Exceptions\CircularReferenceExceptionInterface;

class CircularReferenceWatcherTest extends TestCase
{

    private $watcher;

    public function setUp()
    {
        parent::setUp();
        $this->watcher = new CircularReferenceWatcher();
    }

    public function testCloseNotOpenedContext() {
        $this->expectException(CircularReferenceWatcherExceptionInterface::class);
        $this->watcher->closeContext();
    }

    public function testCloseIncorrectContext() {
        $this->expectException(CircularReferenceWatcherExceptionInterface::class);
        $this->watcher->openContext('SomeContext');
        $this->watcher->closeContext('IncorrectContext');
    }

    /**
     * @dataProvider dataProviderForCircular
     */
    public function testCircularContext($context, $exceptedStack) {

        $this->watcher->openContext('One');
        $this->watcher->openContext('Two');
        $this->watcher->openContext('Three');

        try {
            $this->watcher->openContext($context);
            $this->markAsRisky('Expected exception not fired');
        } catch (CircularReferenceExceptionInterface $e) {
            $this->assertEquals($exceptedStack, $e->getContexts());
        }

    }

    public function dataProviderForCircular() {
        return [
            ['Three', ['One', 'Two', 'Three', 'Three']],
            ['Two', ['One', 'Two', 'Three', 'Two']],
            ['One', ['One', 'Two', 'Three', 'One']]
        ];
    }

    public function testWatcherPositive() {
        $this->watcher->openContext('One');
        $this->watcher->openContext('Two');
        $this->watcher->openContext('Three');

        $this->assertEquals('Three', $this->watcher->closeContext());
        $this->assertEquals('Two', $this->watcher->closeContext());
        $this->assertEquals('One', $this->watcher->closeContext());
    }
}