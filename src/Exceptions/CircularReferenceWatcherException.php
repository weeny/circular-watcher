<?php

namespace Weeny\Lib\CircularWatcher\Exceptions;

use Weeny\Contract\Exceptions\CircularReferenceWatcherExceptionInterface;

class CircularReferenceWatcherException extends \Exception implements CircularReferenceWatcherExceptionInterface
{

}