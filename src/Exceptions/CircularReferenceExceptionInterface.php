<?php
namespace Weeny\Lib\CircularWatcher\Exceptions;

use Weeny\Contract\Exceptions\CircularReferenceWatcherExceptionInterface;

interface CircularReferenceExceptionInterface extends CircularReferenceWatcherExceptionInterface
{
    /**
     * Return call queue contexts
     * @return string[]
     */
    public function getContexts(): array;
}