<?php

namespace Weeny\Lib\CircularWatcher\Exceptions;

class CircularReferenceException extends CircularReferenceWatcherException implements CircularReferenceExceptionInterface
{

    protected $contexts;

    public function setContextQueue(array $contexts) {
        $this->contexts = $contexts;
    }

    /**
     * @inheritDoc
     */
    public function getContexts(): array
    {
        return $this->contexts;
    }
}