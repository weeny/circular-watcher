<?php

namespace Weeny\Lib\CircularWatcher;

use Weeny\Lib\CircularWatcher\Exceptions\CircularReferenceExceptionInterface;
use Weeny\Lib\CircularWatcher\Exceptions\CircularReferenceWatcherExceptionInterface;

interface CircularReferenceWatcherInterface
{

    /**
     * Start new context group
     * @param string
     * @return void
     * @throws CircularReferenceExceptionInterface
     */
    public function openContext(string $contextName);

    /**
     * Close last opened context.
     * @param string|null $contextName
     * @return string
     * @throws CircularReferenceWatcherExceptionInterface
     */
    public function closeContext(string $contextName = null): string;
}