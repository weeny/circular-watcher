<?php

namespace Weeny\Lib\CircularWatcher;

use Weeny\Lib\CircularWatcher\Exceptions\CircularReferenceException;
use Weeny\Lib\CircularWatcher\Exceptions\CircularReferenceWatcherException;

class CircularReferenceWatcher implements CircularReferenceWatcherInterface
{

    protected $contextStack = [];

    /**
     * @inheritDoc
     */
    public function openContext(string $contextName)
    {
        if ( in_array($contextName, $this->contextStack) ) {
            $exception = new CircularReferenceException('Attempt open context that was previusly opened');
            $queue = $this->contextStack;
            $queue[] = $contextName;
            $exception->setContextQueue($queue);
            throw $exception;
        }

        $this->contextStack[] = $contextName;
    }

    /**
     * @inheritDoc
     */
    public function closeContext(string $contextName = null): string
    {
        if (count($this->contextStack) == 0) {
            throw new CircularReferenceWatcherException('Attempt close not opened context');
        }

        $lastContext = array_pop($this->contextStack);

        if ( !is_null($contextName) && $contextName != $lastContext) {
            throw new CircularReferenceWatcherException('Attempt close incorrect context with name "'.$contextName.'", currently opened is "'.$lastContext.'"');
        }

        return $lastContext;
    }
}